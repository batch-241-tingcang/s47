// The "document" refers to the whole webpage
// The "querySelector" is used to select a specific object (HTML element) from the document (webpage)
const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

const updateFullName = () => {
    let firstName = txtFirstName.value;
    let lastName = txtLastName.value;

    // The innerHTML property sets or returns the HTML content
    spanFullName.innerHTML = `${firstName} ${lastName}`;
    // The "event.target" contains the element where the event happened
    console.log(event.target);
    // event.target.value gets the value of the input object (similar to the txtFirstName.value)
    console.log(event.target.value);
};

// The "addEventListener" is a function that takes two arguments
// string identifying an event (keyup)
// function that the listener will execute once the specified event is triggered (event)
// keyup – fires when you release a key on the keyboard
txtLastName.addEventListener('keyup', updateFullName);
txtFirstName.addEventListener('keyup', updateFullName);



